import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

public class POS {

	private JFrame frmPosTerminali;
	private JTextField datum;
	private JTextField lokacija;
	private JTextField serijskiBR;
	static boolean ax=false;
	String strNaplata = "";
	int sifra1;
	int sifra2;
	public void Prikazi() {
		try {
			
			con = DriverManager.getConnection("jdbc:firebirdsql://localhost:3050//TEST.FDB", "SYSDBA", "masterkey");
			String sqlSelect = "SELECT RB,DATUM, INTERVENCIJE.NAZIV_INTERVENCIJE, BANKE.NAZIV_BANKE, LOKACIJA, SERVISER, SERIJSKIBR, NAPLATA "
					+ "FROM TERMINALI, INTERVENCIJE, BANKE "
					+ "WHERE TERMINALI.BANKA=BANKE.SIFRA "
					+ "AND TERMINALI.VRSTA_INTERVENCIJE=INTERVENCIJE.SIFRA "
					+ "order by RB";
			pst = con.prepareStatement(sqlSelect);
			rs = pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			table.scrollRectToVisible(table.getCellRect(table.getRowCount()-1, 0, true));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					POS window = new POS();
					window.frmPosTerminali.setVisible(true);
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs =null; 
	private JTable table;
	public POS() {
		initialize();
		Prikazi();
		table.scrollRectToVisible(table.getCellRect(table.getRowCount()-1, 0, true));
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPosTerminali = new JFrame();
		frmPosTerminali.setTitle("POS Terminali");
		frmPosTerminali.setBounds(100, 100, 1132, 514);
		frmPosTerminali.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		
		datum = new JTextField();
		datum.setColumns(10);
		
		JLabel lblLokacija = new JLabel("Lokacija:");
		lblLokacija.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		lokacija = new JTextField();
		lokacija.setColumns(10);
		
		JComboBox vrstaIntervencije = new JComboBox();
		vrstaIntervencije.setModel(new DefaultComboBoxModel(new String[] {"VRSTA INTERVENCIJE:", "ZAMENA", "INSTALACIJA", "DEINSTALACIJA", "PREKONFIGURACIJA"}));
		vrstaIntervencije.setToolTipText("VRSTA INTERVENCIJE");
		
		JComboBox banka = new JComboBox();
		banka.setModel(new DefaultComboBoxModel(new String[] {"BANKA:", "INTESA", "KOMBANK", "CREDITAGRICOLE", "AIK", "ERSTE", "RAIFAISEN"}));
		banka.setToolTipText("BANKA:");
		
		JComboBox serviser = new JComboBox();
		serviser.setModel(new DefaultComboBoxModel(new String[] {"SERVISER:", "Sloba", "Nemanja", "Stefan", "Zoki"}));
		serviser.setToolTipText("SERVISER");
		
		JLabel lblSerijskiBr = new JLabel("Serijski br:");
		
		serijskiBR = new JTextField();
		serijskiBR.setColumns(10);

		JComboBox naplata = new JComboBox();
		naplata.setModel(new DefaultComboBoxModel(new String[] {"ZA NAPLATU:", "DA", "NE"}));
		
		/*
		 * D U G M E 
		 * 
		 * Z A
		 * 
		 * U N O S 
		 *
		 */
		JButton btnUnesi = new JButton("UNESI");
		btnUnesi.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnUnesi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					
					String sqlInsert = "INSERT INTO TERMINALI (DATUM, LOKACIJA, VRSTA_INTERVENCIJE, BANKA, SERVISER, SERIJSKIBR, NAPLATA)" +
					"VALUES (?, ? , ?, ?, ?, ?, ?)";
					con = DriverManager.getConnection("jdbc:firebirdsql://localhost:3050//home/djordje/Documents/TEST.FDB", "SYSDBA", "masterkey");	
					if(vrstaIntervencije.getSelectedItem().equals("ZAMENA")) sifra1=1;
					if(vrstaIntervencije.getSelectedItem().equals("INSTALACIJA")) sifra1=2;
					if(vrstaIntervencije.getSelectedItem().equals("DEINSTALACIJA")) sifra1=3;
					if(vrstaIntervencije.getSelectedItem().equals("PREKONFIGURACIJA")) sifra1=4;
					
					if(banka.getSelectedItem().equals("INTESA")) sifra2=1;
					if(banka.getSelectedItem().equals("KOMBANK")) sifra2=2;
					if(banka.getSelectedItem().equals("CREDIAGRICOLE")) sifra2=3;
					if(banka.getSelectedItem().equals("AIK")) sifra2=4;
					if(banka.getSelectedItem().equals("ERSTE")) sifra2=5;
					if(banka.getSelectedItem().equals("RAIFAISEN")) sifra2=6;
					
					
					pst = con.prepareStatement(sqlInsert);
					pst.setString(1, datum.getText());
					datum.setText("");
					pst.setString(2, lokacija.getText());
					lokacija.setText("");
					//pst.setString(3, (String)vrstaIntervencije.getSelectedItem());
					pst.setInt(3,sifra1);
					
					//vrstaIntervencije.setSelectedIndex(0);
					//pst.setString(4, (String)banka.getSelectedItem());
					pst.setInt(4,sifra2);
					//banka.setSelectedIndex(0);
					pst.setString(5, (String)serviser.getSelectedItem());
					//serviser.setSelectedIndex(0);
					pst.setString(6, serijskiBR.getText());
					serijskiBR.setText("");
					pst.setString(7, (String) naplata.getSelectedItem());
					//naplata.setSelectedIndex(0);
					pst.executeUpdate();
					
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
				table.scrollRectToVisible(table.getCellRect(table.getRowCount()-1, 0, true));
				Prikazi();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{},
			},
			new String[] {
			}
		));
		GroupLayout groupLayout = new GroupLayout(frmPosTerminali.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 1028, Short.MAX_VALUE)
					.addGap(20))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
					.addGap(27))
		);
		
		JLabel lblDatum = new JLabel("DATUM:");
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 977, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(4)
							.addComponent(lblDatum)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(datum, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblLokacija, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lokacija, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(vrstaIntervencije, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(banka, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(serviser, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(lblSerijskiBr, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(serijskiBR, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(naplata, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnUnesi, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDatum, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
								.addComponent(datum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblLokacija, GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
								.addComponent(lokacija, GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE))
							.addGap(21))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnUnesi)
								.addComponent(naplata, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(serijskiBR, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
								.addComponent(vrstaIntervencije, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSerijskiBr, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
								.addComponent(serviser, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
								.addComponent(banka))
							.addGap(18)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		frmPosTerminali.getContentPane().setLayout(groupLayout);
		
	}
}
